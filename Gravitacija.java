/**
 * @author Martin Dagarin
 * @author Mustafa Grabus
 * @author Anze Jensterle
 * @version 1
 * @since 25/2/2019
 */

import java.util.Locale;
import java.util.Scanner;

public class Gravitacija {
    
    private static final double GRAVITATIONAL_CONSTANT = 6.674e-11;
    private static final double EARTH_MASS = 5.972e24;
    private static final double EARTH_RADIUS = 6.371e6;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        // Read altitude
        System.out.print("Altitude (meters):");
        double altitude = sc.nextDouble();
        
        // Calculate acceleration
        double acceleration = getAccelerationForAltitude(altitude);
        
        // Print result
        Gravitacija.print(altitude, acceleration);
    }
    
    public static double getAccelerationForAltitude(double altitude){
        return GRAVITATIONAL_CONSTANT * EARTH_MASS / (EARTH_RADIUS + altitude) / (EARTH_RADIUS + altitude);
    }

    private static void print(double altitude, double acceleration) {
        System.out.printf(Locale.getDefault(), "Na nadmorski visini %1$,.2f m je g = %2$,.2f ms^-2.%n", altitude, acceleration);
    }
}