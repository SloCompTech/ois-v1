# OIS V1

## Access & License

**READ THIS SECTION CAREFULLY !!!**  

Authorized personel to access this repository are:  

- Martin Dagarin
- Mustafa Grabus
- Anze Jensterle
- doc. dr. Dejan Lavbič
- people authorized by people above

If you **DON'T** fit above criteria **STOP BROWSING REPO IMMEDIATELY**, **DESTORY ALL RESOURCES THAT REFERENCE THIS REPO**, **CLOSE THIS PAGE** and **CLEAN BROWSER HISTORY**.  

For license see [License](LICENSE).  

## Usage

``` bash
# Build project
make build

# Run project
make run

# Build & run
make

# Clean repo
make clean
```